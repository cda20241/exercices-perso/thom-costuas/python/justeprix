import random
import time

class JustePrix:
    """
    JustePrix Game

    This class represents a simple number-guessing game called JustePrix. The game allows players to guess a randomly generated number within a specified range.

    Methods:
    - __init__: Initializes the game parameters.
    - setValeurMin: Sets a new minimum value for the guessing range.
    - setValeurMax: Sets a new maximum value for the guessing range.
    - getMinvaleur: Retrieves the current minimum value of the range.
    - getMaxvaleur: Retrieves the current maximum value of the range.
    - generateRandomValue: Generates a random number within the specified range.
    - getRandomValue: Retrieves the generated random number.
    - setPlayerName: Sets the player's name for the game.
    - getPlayerName: Retrieves the player's name.
    - addResults: Adds the game result to the "Resultats.txt" file.
    - getResults: Displays the top 10 winners from the "Resultats.txt" file.
    - searchResults: Retrieves all results from the "Resultats.txt" file.
    - PlusOuMoins: Determines if the player's guess is higher, lower, or equal to the random number.
    - startChrono: Starts the timer for the game.
    - winner: Records the end time and calculates the player's score.
    - getScore: Retrieves the player's score.

    Note:
    - The game results are stored in a file named "Resultats.txt".
    - The maximum and minimum values for the random number can be modified.
    - The game tracks the player's time and displays the top 10 winners.
    - Options include starting a new game, modifying game options, viewing the leaderboard, and quitting the game.
    """

    def __init__(self):
        self.valeurMin = 0
        self.valeurMax = 100
        self.playerName = ""
        self.randomValue = None
        self.chronoDebut = 0
        self.score = 0
 #s
    def setValeurMin(self, newMin):
        newMin= int(newMin)
        if newMin < self.valeurMax:
            self.valeurMin = newMin
            print("\nNouvelle valeur minimale modifiée avec succès\n")
        else:
            raise Exception("InvalidMinValue, value is Superior to max value")

    def setValeurMax(self, newMax):
        newMax = int(newMax)
        if newMax > self.valeurMin:
            self.valeurMax = newMax
            print("\nNouvelle valeur maximale modifiée avec succès\n")
        else:
            raise Exception("InvalidMaxValue, value is Inferior to max value")

    def getMinvaleur(self):
        return self.valeurMin

    def getMaxvaleur(self):
        return self.valeurMax

    def generateRandomValue(self):
        self.randomValue = random.randint(self.valeurMin, self.valeurMax)

    def getRandomValue(self):
        return self.randomValue

    def setPlayerName(self, newPlayerName):
        self.playerName = newPlayerName

    def getPlayerName(self):
        return self.playerName

    def addResults(self, result):
        fichier = open("Resultats.txt", "a")
        fichier.write(result)
        fichier.close()

    def getResults(self):
        with open("Resultats.txt") as f:
            data = f.readlines()[-10:]
        for ligne in data:
            print(ligne.strip())

    def searchResults(self):
        with open("Resultats.txt") as f:
            data = f.readlines()
        return data

    def PlusOuMoins(self, value=None):
        if value==None:
            pass

        value = int(value)
        if value > self.randomValue:
            print("C'est moins \n")
            return False

        elif value < self.randomValue:
            print("C'est plus \n")
            return False
        else:
            self.winner()
            print("C'est gagné \n")
            return True

    def startChrono(self):
        self.chronoDebut = time.time()
    def winner(self):
        end = time.time()
        self.score = end - self.chronoDebut

    def getScore(self):
        return self.score




def clear_screen():
    print("\n" * 7)




newGame = JustePrix()

exit = False
print("Bienvenue dans le jeu du Juste Prix ! \n")
print("Voici nos 10 derniers gagnants : \n")
newGame.getResults()

while not exit:
    print("\n")
    print("1. Lancer une partie")
    print("2. Modifier les options")
    print("3. Consulter le tableau des scores")
    print("4. Quitter la partie")
    selectedOption = str(input(("\nChoissisez une option valide : ")))

    if selectedOption == "1":
        clear_screen()
        ok = False
        while not ok:
            name = str(input(("\nChoissisez votre nom (la partie démarrera après la selection du nom) : ")))

            if name == "exit":
                print("Ce nom est invalide ! ")

            else:
                ok = True
                newGame.setPlayerName(name)


        clear_screen()
        newGame.generateRandomValue()
        newGame.startChrono()
        win = False

        while win==False:
            clear_screen()
            tentative = input("Votre choix ---> ")
            win = newGame.PlusOuMoins(tentative)
        newGame.addResults(newGame.getPlayerName()+ " : " +str(newGame.getScore())+"\n")
        print(newGame.getPlayerName()+ " : " +str(newGame.getScore()))

    elif selectedOption == "2":
        clear_screen()
        exitOptions = False
        print("Que voulez vous modifier ? \n")

        while not exitOptions:
            print("1. Modifier valeur maximale")
            print("2. Modifier valeur minimale")
            print("3. Revenir au menu")

            selectedOption = str(input(("\n Choissisez une option valide : ")))

            if selectedOption == "1":
                val = input("\n Nouvelle valeur maximale : ")
                newGame.setValeurMax(val)
                exitOptions = True

            elif selectedOption == "2":
                val = input("\n Nouvelle valeur minimale : ")
                newGame.setValeurMin(val)
                exitOptions = True

            elif selectedOption == "3":
                exitOptions = True
                clear_screen()

            else:
                print("\nChoix invalide ! \n")

    elif selectedOption == "3":
        clear_screen()
        exitResultat = False
        print("\n\nMenu de recherche des résultats: \n")

        data = newGame.searchResults()

        while not exitResultat:
            recherche = input("\nVotre recherche (tapez 'exit' pour quitter) : ")
            if recherche.lower() == 'exit':
                exitResultat = True
                clear_screen()
            else:
                resultats_trouves = [ligne.strip() for ligne in data if recherche.lower() in ligne.lower()]

                if resultats_trouves:
                    print()
                    for resultat in resultats_trouves:
                        print(resultat)
                else:
                    print("Aucun résultat trouvé.")

    elif selectedOption == "4":
        print("\nAu revoir")
        exit = True

    else:
        print("\nChoix invalide ! \n")








